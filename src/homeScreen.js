import React from "react";
import { Button, View } from "react-native";
import { NavigationContainer, useNavigation } from '@react-navigation/native';


const HomeScreen = () => {
    const navigation = useNavigation(); 

    const handleUsers = () => {
        navigation.navigate("Users");
    };

    return (
        <View> 
            <Button title="Ver Usuários" onPress={handleUsers} /> 
        </View>
    );
};

export default HomeScreen;
